/etc/motd:
  file.managed:
    - source: salt://motd
    - mode: 644
    - user: root
    - group: root